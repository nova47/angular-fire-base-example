// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyAdd2svCPPtzbiC1byVj8Hr4cS4-AhM898',
    authDomain: 'crud-example-c3afe.firebaseapp.com',
    databaseURL: 'https://crud-example-c3afe.firebaseio.com',
    projectId: 'crud-example-c3afe',
    storageBucket: 'crud-example-c3afe.appspot.com',
    messagingSenderId: '1035967883608',
    appId: '1:1035967883608:web:fb42f1069c57ded36bdd2d'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
